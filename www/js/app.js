angular.module('acc-buddy-mobile', ['ionic', 'ngIOS9UIWebViewPatch', 'acc-buddy-mobile.controllers', 'ngCordova', 'ionic.service.core',
  'ionic.service.push', 'acc-buddy-mobile.services', 'acc-buddy-mobile.directives', 'ngResource', 'ngStorage'])
.constant('SERVER', {
  // if using local server
  //url: ''

  // if using our public heroku server
  //url: 'http://acc-buddy-demo.herokuapp.com'
  url: 'http://10.1.140.145:9000'
  //url: 'http://192.168.1.71:9000'
})
.constant('STATE', {
  App: 'app',
  Appointment: 'app.appointments',
  Exercise: 'app.exercises',
  ActivityMonitor: 'app.activityMonitor',
  Service: 'app.services',
  LiveCheckIn: 'app.liveCheckIn',
  Setting: 'app.settings',
  Help: 'app.help',
  Login: 'app.login',
  NoConnection: 'app.noconnection'
})
.run(function($ionicPlatform, $rootScope, $ionicPush, $http, $state, $location, STATE, Buddy, $ionicPopup) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)

    if (window.cordova && window.cordova.plugins.Keyboard) {
      /*
       * The keyboard accessory bar with Done Buttons when on
       * Not working correctly across devices.
       * Works fine on iPhone 6, but the login screen disappears after hit Done key
       */

      //cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      //cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      //StatusBar.styleDefault();
      StatusBar.styleBlackOpaque();

    }
    if(window.PushNotification) {
      var push = PushNotification.init({
        ios: {
            alert: "true",
            badge: "true",
            sound: "true"
        }
      });

      push.on('registration', function(data) {
          Buddy.setDeviceToken(data.registrationId);
      });

      push.on('notification', function(data) {
           var alertPopup = $ionicPopup.alert({
              title: 'ACC Buddy',
              template: data.message
            });
            Buddy.forceReload = true;
            alertPopup.then(function(res) {
              $state.go($state.current.name, {}, {reload: true});
           });
      });

      push.on('error', function(err) {
          console.log('err while registering for notification: ' + JSON.stringify(err));
      });
    }
  });
})

.config(function($stateProvider, $urlRouterProvider, $ionicAppProvider, $httpProvider, $ionicConfigProvider, STATE) {
  // to disable swipe to go back
  $ionicConfigProvider.views.swipeBackEnabled(false);

  $ionicAppProvider.identify({
    app_id: '8ff33942',
    api_key: '6920743909c96355086ee67590caac49f422fed5399ab469',
    dev_push:false
  });

  $stateProvider
    .state(STATE.App, {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html'
  })
  .state(STATE.Login, {
    url: '/login',
    views: {
      'menuContent': {
        templateUrl: 'templates/login.html'
      }
    }
  })
  .state(STATE.Appointment, {
    url: '/appointments',
    views: {
      'menuContent': {
        templateUrl: 'templates/appointments.html',
        controller: 'AppointmentCtrl',
        controllerAs: 'vmAppointment'
      }
    }
  })
  .state(STATE.Exercise, {
    url: '/exercises',
    views: {
      'menuContent': {
        templateUrl: 'templates/exercises.html',
        controller: 'ExcerciseCtrl',
        controllerAs: 'vmExercise'
      }
    }
  })
  .state(STATE.ActivityMonitor, {
    url: '/activityMonitor',
    views: {
      'menuContent': {
        templateUrl: 'templates/activityMonitor.html',
        controller: function(){}
      }
    }
  })
  .state(STATE.Service, {
    url: '/services',
    views: {
      'menuContent': {
        templateUrl: 'templates/services.html',
        controller: function(){}
      }
    }
  })
  .state(STATE.LiveCheckIn, {
    url: '/liveCheckIn',
    views: {
      'menuContent': {
        templateUrl: 'templates/liveCheckIn.html',
        controller: function(){}
      }
    }
  })
  .state(STATE.Setting, {
    url: '/settings',
    views: {
      'menuContent': {
        templateUrl: 'templates/settings.html',
        controller: function(){}
      }
    }
  })
  .state(STATE.Help, {
    url: '/help',
    views: {
      'menuContent': {
        templateUrl: 'templates/help.html',
        controller: function(){}
      }
    }
  })
  .state(STATE.NoConnection, {
    url: '/noconnection',
    views: {
      'menuContent': {
        templateUrl: 'templates/noconnection.html',
        controller: 'AppCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  //$urlRouterProvider.otherwise('app.login');
})

.filter('trustAsResourceUrl', ['$sce', function($sce) {
  // this to allow set url from angularjs to iframe
  return function(val) {
      return $sce.trustAsResourceUrl(val);
  };
}]);
