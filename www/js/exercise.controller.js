angular.module('acc-buddy-mobile.controllers')
.controller('ExcerciseCtrl', function(Buddy, $scope, $state, STATE) {
  var vm = this;

  vm.toggleDetailPane = toggleDetailPane;
  vm.selectExerciseCheckbox = selectExerciseCheckbox;
  vm.exercises = Buddy.exercises;

  $scope.$on('$ionicView.beforeEnter', function() {
    if($state.current.name == STATE.Exercise) {
      // applies when the application just started
      if(!Buddy.dataPopulated || Buddy.forceReload) {
        Buddy.loadClientData().then(function() {
          vm.exercises = Buddy.exercises;
          Buddy.forceReload = false;
          Buddy.dataPopulated = true;
        });
      }
    }
  });

  // only one exercise detail pane is opened at a time
  function toggleDetailPane($event, exercise) {
    var showDetails = exercise.showDetails || false;
    var exercises = vm.exercises;
    for(var i = 0; i < exercises.length; i++) {
      exercises[i].showDetails = false;
    }
    exercise.showDetails = !showDetails;
    $event.stopPropagation();
  }

  function selectExerciseCheckbox($event, exercise) {
    $event.stopPropagation();
  }
});
