'use strict';

angular.module('acc-buddy-mobile.directives', [])
.directive('progressBar', function() {
  return {
    restrict: 'AE',
    replace: 'true',
    templateUrl: 'templates/progressbar.html'
  };
});
