'use strict';

angular.module('acc-buddy-mobile.directives')
.directive('headerBar', function() {
  return {
    restrict: 'AE',
    replace: 'true',
    templateUrl: 'templates/headerbar.html'
  };
});
