'use strict';

angular.module('acc-buddy-mobile.directives')
.directive('ngEnter', function() {
  /*
   * This directive is to capture the enter key on the password field and execute the function assigned to ng-enter attribute
   * Only works on the mac safari
   */
  return function(scope, element, attrs) {
    element.bind("keydown keypress", function(event) {
      if(event.which === 13) {
        scope.$apply(function() {
          scope.$eval(attrs.ngEnter);
        });
        event.preventDefault();
      }
    });
  };
})
.directive('autoFocus', function($timeout) {
  /*
   * This directive is to set auto focus for an element
   * Only works on the mac safari
   */
  return {
    link: function(scope, element, attrs) {
      $timeout(function() {
        element[0].focus();
      }, 150);
    }
  };
});
