'use strict';

angular.module('acc-buddy-mobile.directives', [])
.directive('messageBar', function() {
  return {
    restrict: 'AE',
    replace: 'true',
    templateUrl: 'templates/messagebar.html'
  };
});
