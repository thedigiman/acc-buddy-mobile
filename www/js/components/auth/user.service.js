'use strict';

angular.module('acc-buddy-mobile.services')
  .factory('User', function ($resource, SERVER) {
    var USER_ENDPOINT = '/api/users/:id/:controller';
    return $resource(SERVER.url + USER_ENDPOINT, {
      id: '@_id'
    },
    {
      changePassword: {
        method: 'PUT',
        params: {
          controller:'password'
        }
      },
      get: {
        method: 'GET',
        params: {
          id:'me'
        }
      }
	  });
  });
