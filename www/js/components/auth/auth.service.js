'use strict';

angular.module('acc-buddy-mobile.services', ['acc-buddy-mobile', 'ngResource', 'ngStorage'])
  .factory('Auth', function Auth($http, $q, SERVER, $localStorage) {
    var TOKEN_KEY = 'acc_buddy_token';
    var USER_KEY = 'acc_buddy_user';
    var AUTH_ENDPOINT = '/auth/local';

    var currentUser = $localStorage.USER_KEY;

    var self = this;

    self.storeUserData = storeUserData;
    self.clearUserData = clearUserData;
    self.login = login;
    self.logout = logout;
    self.getCurrentUser = getCurrentUser;
    self.isAdmin = isAdmin;
    self.getToken = getToken;
    self.isLoggedIn = isLoggedIn;

    return self;

    function isLoggedIn() {
      return (self.getCurrentUser() != null);
    };

    function storeUserData(data) {
      $localStorage.TOKEN_KEY = data.token;
      $localStorage.USER_KEY = data.user;
      currentUser = $localStorage.USER_KEY;
    };

    function clearUserData() {
      delete $localStorage.USER_KEY;
      delete $localStorage.TOKEN_KEY;
      currentUser = null;
    };

    function login(user) {
      return $http.post(SERVER.url + AUTH_ENDPOINT, {email: user.email, password: user.password});
    };

    function logout() {
      clearUserData();
    };

    function getCurrentUser() {
      return currentUser;
    };

    function isAdmin() {
      return currentUser.role === 'admin';
    };

    function getToken() {
      return $localStorage.TOKEN_KEY;
    };
});
