'use strict';

angular.module('acc-buddy-mobile.services')
  .factory('Buddy', function Buddy($http, SERVER, $localStorage) {

    var APPOINTMENT_ENDPOINT = '/api/v1/clients/getClientData/';
    var APPOINTMENT_REQUEST_ENDPOINT = '/api/v1/appointmentRequests/';
    var APPOINTMENT_KEY = 'acc_buddy_appointments';
    var EXERCISE_KEY = 'acc_buddy_exercises';
    var DEVICE_TOKEN = 'acc_buddy_device_token';

    var self = {
      clientName: 'Sarah Marshall',
      clientNumber: 'PN1000001',
      clientId: '56c511400fe17082c01688ac',
      appointments: [],
      exercises: [],
      loadClientData: loadClientData,
      requestAppointment: requestAppointment,
      cacheData: cacheData,
      setDeviceToken: setDeviceToken,
      isLoading: false,
      forceReload: false,
      clearUserData: clearUserData,
      dataPopulated: false
    };

    init();

    return self;

    function init() {
      self.deviceToken = $localStorage.DEVICE_TOKEN || '';
      self.appointments = $localStorage.APPOINTMENT_KEY || [];
      self.exercises = $localStorage.EXERCISE_KEY || [];
    };

    function loadClientData() {
      if(self.isLoading) {
        return;
      }
      self.isLoading = true;
      return $http.get(SERVER.url + APPOINTMENT_ENDPOINT + self.clientNumber).then(function(result) {
        self.isLoading = false;
        self.clientId = result.data._id;
        self.appointments = result.data.appointments;
        self.exercises = result.data.exercises;

        self.cacheData(result.data);
      }, function(err) {
        self.isLoading = false;
      });
    };

    function requestAppointment(appointmentRequest) {
      return $http.post(SERVER.url + APPOINTMENT_REQUEST_ENDPOINT, {appointmentRequest: appointmentRequest});
    };

    function cacheData(data) {
      $localStorage.APPOINTMENT_KEY = data.appointments;
      $localStorage.EXERCISE_KEY = data.exercises;
    };

    function setDeviceToken(newDeviceToken) {
      self.deviceToken = newDeviceToken;
      $localStorage.DEVICE_TOKEN = newDeviceToken;
    };

    function clearUserData() {
      self.dataPopulated = false;
      self.forceReload = false;
      self.appointments = [];
      self.exercises = [];
      $localStorage.APPOINTMENT_KEY = [];
      $localStorage.EXERCISE_KEY = [];
    };
  });
