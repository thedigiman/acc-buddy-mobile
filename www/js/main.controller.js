angular.module('acc-buddy-mobile.controllers', ['ionic', 'ngMaterial', 'ngAria', 'ngMessages', 'acc-buddy-mobile.services', 'acc-buddy-mobile', 'ngTouch', 'acc-buddy-mobile.directives', 'ngCordova'])

.controller('AppCtrl', function($scope, $state, $ionicSideMenuDelegate, Auth, Buddy, $ionicLoading, STATE) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:

  var highlightClassNames = ['highlightAppointment', 'highlightExercise', 'highlightActivity', 'highlightService', 'highlightLiveCheckin'];
  var stateNames = ['app.appointments', 'app.exercises', 'app.activityMonitor', 'app.services', 'app.liveCheckIn'];

  var vm = this;
  // Form data for the login modal
  vm.loginData = {};
  vm.client = {};
  vm.loginData.username = 'Sarah';
  vm.passwordInvalid = false;

  vm.login = login;
  vm.logOut = logOut;

  vm.toggleLeft = toggleLeft;
  vm.selectMenuItem = selectMenuItem;
  vm.selectOtherMenuItem = selectOtherMenuItem;
  vm.checkConnection = checkConnection;

  $scope.$on('$stateChangeStart', stateChangeStartHandler);
  document.addEventListener("offline", onOffline, false);
  document.addEventListener("online", onOnline, false);

  init();

  function init() {
    var clientName = Buddy.clientName.split(' ');
    vm.client.firstName = clientName[0];
    vm.client.lastName = clientName[1];

    if(Auth.getCurrentUser() !== null) {
      $state.go(STATE.Appointment);
    } else {
      $state.go(STATE.Login);
    }
  }

  function login() {
    $ionicLoading.show({
      template: 'Logging in...'
    });
    Auth.login({
      email: 'admin@admin.com',
      password: 'password'
    })
    .then(function(data) {
      Auth.storeUserData(data);
      vm.passwordInvalid = false;
      $ionicLoading.hide();
      $state.go(STATE.Appointment);
    })
    .catch( function(err) {
      Auth.clearUserData();
      vm.passwordInvalid = true;
      $ionicLoading.hide();
    });
  }

  function logOut() {
    Auth.logout();
    Buddy.clearUserData();
    $state.go(STATE.Login);
  }

  function toggleLeft() {
    var isOpen = $ionicSideMenuDelegate.isOpen();
    $ionicSideMenuDelegate.toggleLeft();
    if(!isOpen) {
      for(var i = 0; i < stateNames.length; i++) {
        vm[highlightClassNames[i]] = false;
        if($state.current.name == stateNames[i]) {
          vm[highlightClassNames[i]] = true;
        }
      }
    }
  }

  function selectMenuItem(stateName) {
    $state.go('app.' + stateName);
  }

  function selectOtherMenuItem($event) {
    if($event.target.id === 'settingsMenu') {
      $state.go(STATE.Setting);
    } else if($event.target.id === 'helpMenu') {
      $state.go(STATE.Help);
    }
  }

  /*
   * This function is to be used on the noconnection page
   */


  function checkConnection() {
    if(!navigator || !navigator.network) {
      return;
    }
    var isOffline = navigator.network.connection.type == Connection.NONE;
    if(!isOffline) {
      $state.go(vm.lastState);
    }
  }

  /*
   * Handles no-connection mode here. All state change request will be falled back to app.noconnection stateName
   * Auto-redirect to the last state page when the device is back online
   */


 function stateChangeStartHandler($event,next, nextParams, fromState) {
   var isOffline = false;
   if(navigator && navigator.network && navigator.network.connection) {
     isOffline = navigator.network.connection.type == Connection.NONE;
   }


   if($state.current.name !== STATE.NoConnection) {
     vm.lastState = $state.current.name;
   }

   if(isOffline && next.name !== STATE.NoConnection) {
     $event.preventDefault();
     $state.go(STATE.NoConnection);
   } else if(!isOffline && next.name == STATE.NoConnection) {
     $event.preventDefault();
     vm.lastState = vm.lastState || STATE.Login;
     $state.go(vm.lastState);
   }
  }


  function onOffline() {
    if($state.current.name !== STATE.NoConnection) {
      vm.lastState = $state.current.name;
    }
    $state.go(STATE.NoConnection);
  }

  function onOnline() {
      vm.lastState = vm.lastState || STATE.Login;
      $state.go(vm.lastState);
  }
});
