angular.module('acc-buddy-mobile.controllers')
.controller('AppointmentCtrl', function(Buddy, $scope, STATE, $state, $ionicLoading) {
  var DATE_FORMAT = 'ddd DD MMM';
  var vm = this;

  vm.toggleDetailPane = toggleDetailPane;
  vm.formatDate = formatDate;
  vm.requestAppointment = requestAppointment;

  vm.appointments = Buddy.appointments;

  $scope.$on('$ionicView.beforeEnter', function() {
    if ($state.current.name == STATE.Appointment) {
      // applies when the application just started, or force to reload data after receiving notification
      if(!Buddy.dataPopulated || Buddy.forceReload) {
        Buddy.loadClientData().then(function() {
          vm.appointments = Buddy.appointments;
          Buddy.forceReload = false;
          Buddy.dataPopulated = true;
        });
      }

    }
  });

  function toggleDetailPane($event, appointment) {
    var showDetails = appointment.showDetails || false;
    var appointments = vm.appointments;
    for(var i = 0; i < appointments.length; i++) {
      appointments[i].showDetails = false;
    }
    appointment.showDetails = !showDetails;
    $event.stopPropagation();
  }

  function formatDate(appointmentDate) {
    return moment(appointmentDate).format(DATE_FORMAT);
  }

  function requestAppointment() {

    var appointmentRequest = {clientId: Buddy.clientId, clientName: Buddy.clientName, clientNumber: Buddy.clientNumber,
        requestTime: new Date(), deviceToken: Buddy.deviceToken};

    $ionicLoading.show({
      template: 'Sending appointment request...'
    });

    Buddy.requestAppointment(appointmentRequest).then(function(response) {
      $ionicLoading.hide();
    }, function(err) {
      console.log('ops. something went wrong. cannot create a new appointment request. err = ' + JSON.stringify(err));
      $ionicLoading.hide();
    });
  }
});
