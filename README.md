Ionic App Base
=====================

A starting project for Ionic that optionally supports using custom SCSS.

## Using this project

We recommend using the [Ionic CLI](https://github.com/driftyco/ionic-cli) to create new Ionic projects that are based on this project but use a ready-made starter template.

For example, to start a new Ionic project with the default tabs interface, make sure the `ionic` utility is installed:

```bash
$ npm install -g ionic
```

Then run:

```bash
$ ionic start myProject tabs
```

More info on this can be found on the Ionic [Getting Started](http://ionicframework.com/getting-started) page and the [Ionic CLI](https://github.com/driftyco/ionic-cli) repo.

## Issues
Issues have been disabled on this repo, if you do find an issue or have a question consider posting it on the [Ionic Forum](http://forum.ionicframework.com/).  Or else if there is truly an error, follow our guidelines for [submitting an issue](http://ionicframework.com/submit-issue/) to the main Ionic repository.


## To configure proxy
"path": "/auth/local",
"proxyUrl": "http://acc-buddy-demo.herokuapp.com/auth/local"

or
,
"proxies": [
  {
    "path": "/api",
    "proxyUrl": "http://192.168.1.70:9000/api"
  }
]

## To test the app on Browser
set the SERVER.url = ''
set the proxies as
"proxies": [
  {
    "path": "/api",
    "proxyUrl": "http://localhost:9000/api"
  },
  {
    "path": "/auth/local",
    "proxyUrl": "http://localhost:9000/auth/local"
  }
]

## To test the app on simulator or actual Device
set the SERVER.url = http://192.168.1.70:9000
remove the proxy

## Certificates, and provisioning profile of the acc buddy app
Go to Apple developer Solnet account, and look for:
iOS Certificate: nz.co.solnetsolutions.digital.acc-buddy-mobile
Provisioning Profile: acc buddy mobileProfile
At this moment the provisioning profile includes Helen phone and Solnet iPhone 6. Add any device you want to install into this profile, then re-install the provisioning profile onto your mac, to install the app

## Changes in XCode
Under general tab:
  Set Bundle identifier as: nz.co.solnetsolutions.digital.acc-buddy-mobile
  Team: Solnet Solutions Ltd
  Deployment Target: 9.2
  Devices: iPhone
  Requires fullscreen: Checked
Under Capabilities tab:
  Enable Push notification
  Background modes: Select Remote notifications
Under Build settings:
  Build options:
      Enable Bitcode: No
